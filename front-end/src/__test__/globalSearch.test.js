import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import Search from '../components/pages/search/Search'
import SearchCityCard from  '../components/pages/search/SearchCityCard'
import SearchCovidCard from  '../components/pages/search/SearchCovidCard'
import SearchFlightCard from  '../components/pages/search/SearchFlightCard'
configure({ adapter: new Adapter() });
describe("Test Global Search Page" , () =>{
  test('Global Search Page exist',() => {
      const ccity = shallow(<Search />)
      expect(ccity).not.toBeNull()
  });
  test('Global Search Page', () => {
      const ccity = shallow(<Search />)
      expect(ccity).toBeDefined()
  });
  test('Global Search Page', async () => {
      const copy = shallow(<Search />)
      expect(copy).toMatchSnapshot()
  });
});

describe("Test global search search cards" , () =>{
  test('Global Search SearchCityCard', async () => {
      const input = []
      const count = 0
      const ccopy = shallow(<SearchCityCard
        cityData={input}
        cityCount={count}
        searchTerm="austin"
        />)
      expect(ccopy).toMatchSnapshot()
  });
  test('Global Search SearchCovidCard', async () => {
      const input = []
      const count = 0
      const cocopy = shallow(<SearchCovidCard
        covidData={input}
        covidCount={count}
        searchTerm="austin"
        />)
      expect(cocopy).toMatchSnapshot()
  });
  test('Global Search SearchFlightCard', async () => {
      const input = []
      const count = 0
      const fcopy = shallow(<SearchFlightCard
        flightData={input}
        flightCount={count}
        searchTerm="austin"
        />)
      expect(fcopy).toMatchSnapshot()
  });
});
