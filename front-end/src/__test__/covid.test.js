import Covid from '../components/pages/covid/Covid.js'
import React from "react"
import { configure, shallow } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import { Router } from 'react-router';
import { Route } from 'react-router-dom';
import { useQueryParam, NumberParam, QueryParamProvider } from 'use-query-params';
configure({ adapter: new Adapter() });
describe("Test Covid module page" , () =>{
  test('Covid Page exist',() => {
      const ccity = shallow(<QueryParamProvider><Covid /></ QueryParamProvider>)
      expect(ccity).not.toBeNull()
  });
  test('Covid Page defined', () => {
      const ccity = shallow(<QueryParamProvider><Covid /></ QueryParamProvider>)
      expect(ccity).toBeDefined()
  });
  test('Covid page', async () => {
      const copy = shallow(<QueryParamProvider><Covid /></ QueryParamProvider>)
      expect(copy).toMatchSnapshot()
  });
});
