import React, {Component} from "react";
import {Link} from "react-router-dom";
import Highlighter from "react-highlight-words";

class Table extends Component {
    state = {};

    render() {
        let searchWords = this.props.searchTerms ? this.props.searchTerms.split(" ") : [];

        return (
            <div style={{'fontSize': 13}}>

                <table className="table table-condensed table-hover">
                    <thead>
                    <tr>
                        <th scope="col">County Name</th>
                        <th scope="col">Cases</th>
                        <th scope="col">Deaths</th>
                        <th scope="col">Infection Rate</th>
                        <th scope="col">Risk Level</th>
                        <th scope="col">Transmission Level (CDC)</th>
                        <th scope="col">ICU Bed Capacity</th>
                        <th scope="col">ICU Bed Usage</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.stats.map(row =>
                        <tr key={row.fips}>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row.county_name}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.cases ? ' ' + row.cases : ''}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.deaths ? ' ' + row.deaths : ''}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.infection_rate ? ' ' + row.infection_rate : ''}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.risk_level ? ' ' + row.risk_level : ''}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.cdc_transmission_level ? ' ' + row.cdc_transmission_level : ''}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.icu_bed_capacity ? ' ' + row.icu_bed_capacity : ''}
                            /></td>
                            <td><Highlighter
                                searchWords={searchWords}
                                textToHighlight={row?.icu_bed_current_usage ? ' ' + row.icu_bed_current_usage : ''}/>
                            </td>
                            <td>
                                <Link to={"/covid/" + row.county_name.replace('County', '')}>
                                    View
                                </Link>
                            </td>
                        </tr>)}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Table;