import React from 'react'
import { Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import Highlighter from 'react-highlight-words';
import './GlobalSearch.css';

function SearchCityCard({cityData, cityCount, searchTerm}) {
    console.log(cityData);
    const searchWords = searchTerm ? searchTerm.split(" ") : [""];

    // Need to change to get localStorage logic
    const countRemaining = cityCount > 10 ? cityCount - 10 : 0;

    return (
        <Row id="hoverable">
            {cityData.map(data => (
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card className="searchCard">
                        <Card.Body>
                            <Card.Title>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.city_name}
                                />
                            </Card.Title>
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><b>Population: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.population ? 
                                        ' '+data.population : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Latitude: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.Latitude ? 
                                        ' '+data.Latitude : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Longitude: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.Longitude ? 
                                        ' '+data.Longitude : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>County: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.County_name ? 
                                        ' '+data.County_name : ' Unknown'}
                                />
                            </ListGroupItem>
                        </ListGroup>
                        <a href={"/cityModule/"+data.city_name} class="stretched-link"> </a>
                    </Card>
                </Col>
            ))}
            {countRemaining !== 0 ? 
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card className="searchCard" style={{ height: '100%', width: '100%' }}>
                        <Card.Body>
                            <Card.Title>
                                See {countRemaining} more City search results.
                            </Card.Title>
                        </Card.Body>
                        <a href="/citySearch" class="stretched-link"/>
                    </Card>
                </Col> 
                : <></>}
        </Row>
    )
}

export default SearchCityCard;