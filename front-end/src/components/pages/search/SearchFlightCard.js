import React from 'react'
import { Card, Col, ListGroup, ListGroupItem, Row } from 'react-bootstrap';
import Highlighter from 'react-highlight-words';

function SearchFlightCard({flightData, flightCount, searchTerm}) {
    console.log(flightData);
    const searchWords = searchTerm ? searchTerm.split(" ") : [""];

    // Need to change to get localStorage logic
    const countRemaining = flightCount > 10 ? flightCount - 10 : 0;

    return (
        <Row id="hoverable">
            {flightData.map(data => (
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card className="searchCard">
                        <Card.Body>
                            <Card.Title>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={`${data?.airline_name} ${data?.flight_number}`}
                                />
                            </Card.Title>
                        </Card.Body>
                        <ListGroup className="list-group-flush">
                            <ListGroupItem><b>Departure Code: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.departure_icao ? 
                                        ' '+data.departure_icao : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Departure Time: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.departure_scheduled ? 
                                        ' '+data.departure_scheduled : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Arrival Code: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.arrival_icao ? 
                                        ' '+data.arrival_icao : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Arrival Time: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.arrival_scheduled ? 
                                        ' '+data.arrival_scheduled : ' Unknown'}
                                />
                            </ListGroupItem>
                            <ListGroupItem><b>Airline: </b>
                                <Highlighter
                                    highlightClassName="searchHighlight"
                                    searchWords={searchWords}
                                    textToHighlight={data?.airline_name? 
                                        ' '+data.airline_name : ' Unknown'}
                                />
                            </ListGroupItem>
                        </ListGroup>
                        <a href={"/flight/"+data.id} class="stretched-link"> </a>
                    </Card>
                </Col>
            ))}
            {countRemaining !== 0 ? 
                <Col sm={3} style={{marginBottom: '10px'}}>
                    <Card className="searchCard" style={{ height: '100%', width: '100%' }}>
                        <Card.Body>
                            <Card.Title>
                                See {countRemaining} more Flight search results.
                            </Card.Title>
                        </Card.Body>
                        <a href={searchTerm ? 
                        `/flight/?page=1&pagesize=20&s=departure_icaowithasc&f=${searchTerm}`
                        :`/flight/?page=1&pagesize=20&s=departure_icaowithasc`} class="stretched-link"/>
                    </Card>
                </Col> 
                : <></>}
        </Row>
    )
}

export default SearchFlightCard;