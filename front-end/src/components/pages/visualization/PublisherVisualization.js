import React, { useEffect, useState } from 'react';
import {PieChart,Cell, Pie,BarChart,CartesianGrid,XAxis,YAxis,Tooltip,Legend,Bar, ScatterChart,ZAxis,Scatter,RadarChart,PolarGrid,PolarAngleAxis,PolarRadiusAxis,Radar} from 'recharts';
import axios from "axios";
import { Spinner } from 'react-bootstrap';


function PublisherVisualization(){
  const colors = ["darkturquoise","antiquewhite","aqua","aquamarine","gold","hotpink","bisque","indigo","blueviolet","brown","burlywood","cadetblue","chartreuse","coral","mediumorchid","navy","darkcyan"];
  const devData = [{'typename': 'Books', 'amount': 255}, {'typename': 'Textbooks', 'amount': 3}, {'typename': 'Academic publishing', 'amount': 2},  {'typename': 'E-books', 'amount': 2}, {'typename': 'Limited Editions', 'amount': 1}, {'typename': 'periodicals', 'amount': 1}, {'typename': 'Bible', 'amount': 2},  {'typename': 'testing materials', 'amount': 1}, {'typename': 'Audio', 'amount': 1},  {'typename': "children's books", 'amount': 1}, {'typename': 'Comics', 'amount': 2}, {'typename': 'Reference works', 'amount': 1}, {'typename': "Children's Books", 'amount': 1}, {'typename': 'Plays', 'amount': 1},  {'typename': 'Magazines', 'amount': 8}, {'typename': 'digital media', 'amount': 1},  {'typename': 'Journals', 'amount': 23}, {'typename': 'Novels', 'amount': 1}];
  return (
    <div>
    <h3 className="borderedHeader">
        Amount of Publishers by Publication Type
    </h3>
    <br/>
    <div className="d-flex justify-content-center">
    <PieChart width={800} height={1000}>
        <Legend />
        <Pie data={devData} dataKey="amount" nameKey="typename" cx="50%" cy="50%" outerRadius={400} fill="#82ca9d" label>
        {
          devData.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={colors[index]}/>
          ))
        }
      </ Pie>
      </PieChart>
      </ div>
      </ div>
    )
}

export default PublisherVisualization;
