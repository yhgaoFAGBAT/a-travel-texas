import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Spinner } from 'react-bootstrap';
import BubbleChart from '@weknow/react-bubble-chart-d3';

function BookVisualization () {
    const [isLoading, setIsLoading] = useState(true);
    const [displayedData, setDisplayedData] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const response = await axios.get("https://api.prideinwriting.me/api/books");

            // Create mapping for counting number of genres
            const mapping = new Map();
            response.data.data.forEach((book) => {
                let tempGenre = book.genres;
                if (tempGenre !== "nan") {
                    tempGenre = tempGenre.substring(2, tempGenre.length-2);
                }

                if (mapping.get(tempGenre)) {
                    mapping.set(tempGenre, mapping.get(tempGenre) + 1);
                } else {
                    mapping.set(tempGenre, 1);
                }
            });

            // Convert to format [{label:, value:}]
            let temp = Array.from(mapping, ([label, value]) => ({ label, value }));
            setDisplayedData(temp);
            setIsLoading(false);
        }
        getData();
    },[]);

    return (
        <div>
            <h3 class="borderedHeader">Books by Genre</h3>
            <br/>
            {isLoading ? (
                    <div class="d-flex justify-content-center">
                        <Spinner animation="border" role="status">
                            <span className="visually-hidden">Loading...</span>
                        </Spinner>
                    </div>
                ) : (
                    <div class="d-flex justify-content-center">
                    <BubbleChart
                        graph={{
                            zoom: 0.75,
                            offsetX: 0.0,
                            offsetY: 0.0,
                        }}
                        width={1100}
                        height={700}
                        valueFont={{
                            family: "Arial",
                            size: 12,
                            color: "#fff",
                            weight: "bold",
                        }}
                        labelFont={{
                            family: "Arial",
                            size: 16,
                            color: "#fff",
                            weight: "bold",
                        }}
                        data={displayedData}
                    />
                    </div>
                )}
        </div>
    );
}

export default BookVisualization;
