import { Container } from "react-bootstrap";
import BookVisualization from "./BookVisualization";
import AuthorVisualization from "./AuthorVisualization";
import PublisherVisualization from "./PublisherVisualization";


function DevVisualization () {
    return (
        <div>
            <Container>
                <h1>Developer Visualizations</h1>
                <br/>
                <BookVisualization/>
                <br/>
                <AuthorVisualization/>
                <br />
                <PublisherVisualization />
            </Container>
        </div>
    );
}

export default DevVisualization;
