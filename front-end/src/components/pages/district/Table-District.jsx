import React, {Component} from "react";
import {Link} from "react-router-dom";
import {Badge} from "react-bootstrap";

class Table extends Component {
    state = {
        rows: [
            {
                'row': 1,
                'county': 'Travis',
                'page_name' : 'district_instance_1',
                'zip_code': 78705,
                'size_sq': '1,023',
                'latitude': '30° 19\' 48.00" N',
                'longitude': '-97° 46\' 48.00" W',
                'walk_score': 28,
                'bike_score': 32,
                'transit_score': '-',
                'covid_transmission_rate': 'High',
                'disabled': false
            }, {
                'row': 2,
                'county': 'Hays',
                'page_name' : 'district_instance_2',
                'zip_code': 69032,
                'size_sq': '713',
                'latitude': '40° 41\' 37\'\' N',
                'longitude': '101° 13\' 14\'\' W',
                'walk_score': 98,
                'bike_score': 87,
                'transit_score': 99,
                'covid_transmission_rate': 'High',
                'disabled': false
            }, {
                'row': 3,
                'county': 'Harris',
                'page_name' : 'district_instance_3',
                'zip_code': 77007,
                'size_sq': '1,777',
                'latitude': '29° 44\' 59.6652\'\' N',
                'longitude': '95° 21\' 30.3156\'\' W',
                'walk_score': 82,
                'bike_score': 53,
                'transit_score': '-',
                'covid_transmission_rate': 'High',
                'disabled': false
            }
        ]
    };

    /*
    Population
Population density
Household size
Percent uninsured
CDC Social Vulnerability Index
     */

    render() {
        console.log('Table props: ', this.props);

        console.log('Covid stats -> Table: ', this.props.stats);

        return (
            <table className="table table-condensed table-hover" style={{'text-align': 'center'}}>
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">County</th>
                    <th scope="col">Zip Code</th>
                    <th scope="col">Size (sq.)</th>
                    <th scope="col">Latitude</th>
                    <th scope="col">Longitude</th>
                    <th scope="col">Walk Score</th>
                    <th scope="col">Bike Score</th>
                    <th scope="col">Transit Score</th>
                    <th scope="col">Covid Transmission Rate</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                {this.state.rows.map(row =>
                    <tr key={row.row}>
                        <th scope="row">{row.row}</th>
                        <td>{row.county}</td>
                        <td>{row.zip_code}</td>
                        <td>{row.size_sq}</td>
                        <td>{row.latitude}</td>
                        <td>{row.longitude}</td>
                        <td>{row.walk_score}</td>
                        <td>{row.bike_score}</td>
                        <td>{row.transit_score}</td>
                        <td style={{color: 'red'}}> {row.covid_transmission_rate}</td>

                        <td>
                            <Link to={row.page_name} row={row}
                                  className={row.disabled ? "btn btn-secondary btn-sm disabled" : "btn btn-primary btn-sm"}>
                                View
                            </Link>
                        </td>
                    </tr>)}
                </tbody>
            </table>
        )
    }
}

export default Table;