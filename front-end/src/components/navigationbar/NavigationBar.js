import React from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import att_logo from '../../static_resources/att_logo.png'

function NavigationBar() {
    return (
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="/">
                    <img src={att_logo} alt="ATravelTX Logo" style={{height: '35px'}}/>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" >
                <Nav className="me-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/flight">Flight</Nav.Link>
                    <Nav.Link href="/cityMain">City</Nav.Link>
                    <Nav.Link href="/covid">Covid</Nav.Link>
                    <Nav.Link href="/about">About</Nav.Link>
                    <Nav.Link href="/search">Search</Nav.Link>
                    <Nav.Link href="/visualization">Visualizations</Nav.Link>
                    <Nav.Link href="/devVisualization">Developer Visualizations</Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default NavigationBar;
