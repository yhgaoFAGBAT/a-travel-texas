import React from "react";
import './Objectshower.css'

//TODO: Let src accepting image links passed in by url
const Objectshower = ({description, url, title, target}) => {
    return (
    <div class="card card_sup">
        <img class="card-img-top" src={url} alt="Card image cap" />
        <div class="card-body">
            <h5 class="card-title">{title}</h5>
            <p class="card-text">{description}</p>
            <a href= {target} class="btn btn-primary">Learn more</a>
        </div>
    </div>
    )
}

export default Objectshower;
