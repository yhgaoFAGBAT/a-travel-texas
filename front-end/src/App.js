import './App.css';
import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './components/pages/home/Home';
import Flight from './components/pages/flight/Flight';
import District from './components/pages/district/District';
import Covid from './components/pages/covid/Covid';
import About from './components/pages/about/About';
import NoMatch from './components/pages/nomatch/NoMatch';
import NavigationBar from './components/navigationbar/NavigationBar';
import Cityinstance from './components/pages/city/city';
import FlightInstance from './components/pages/flight/FlightInstance';
import CityModule from './components/pages/city/cityModule'
import CitySearch from './components/pages/city/CitySearch'
import CityMain from './components/pages/city/cityMain'
import City from './components/pages/city/city'
import Search from './components/pages/search/Search';

import CovidInstance from './components/pages/covid/covid_instance';
import { QueryParamProvider } from 'use-query-params';
import Visualization from './components/pages/visualization/Visualization';
import DevVisualization from './components/pages/visualization/DevVisualization';

function App() {
  return (
    <React.Fragment>
      <NavigationBar />
        <Router>
          <QueryParamProvider>
          <Switch>
            <Route exact path="/" component={Home} />

            <Route path="/flight/:flightId" component={FlightInstance}/>
            <Route path="/flight" component={Flight} />

            <Route exact path="/district" component={District} />
            <Route exact path="/covid" component={Covid} />
            <Route exact path="/about" component={About} />
            <Route exact path="/search" component={Search}/>

            <Route exact path="/cityModule" component={CityModule} />
            <Route exact path="/citySearch" component={CitySearch} />
            <Route exact path="/cityMain" component={CityMain} />
            <Route path="/cityModule/:city_name" component = {City} />

            <Route exact path="/visualization" component={Visualization}/>
            <Route exact path="/devVisualization" component={DevVisualization}/>

            <Route path="/covid/:countyName" component={CovidInstance} />

            <Route component={NoMatch} />
          </Switch>
          </QueryParamProvider>
        </Router>
    </React.Fragment>
  );
}

export default App;
