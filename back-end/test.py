from unittest import main, TestCase
import unittest
import requests


class Test(unittest.TestCase):
    def test_covid(self):
        r = requests.get("https://api.atraveltx.me/api/covid")
        self.assertEqual(r.status_code, 200)
        data = r.json()
        self.assertEqual(len(data["result"]), 254)

    def test_covid_name(self):
        r = requests.get("https://api.atraveltx.me/api/covid/county_name=Harris")
        data = r.json()
        expect = {
            "case_density": 19.0,
            "cases": 546123,
            "cdc_transmission_level": "High",
            "cdc_transmission_level_IGNORE": 3,
            "cities": [
                "Alief",
                "Barker",
                "Baytown",
                "Bellaire",
                "Channelview",
                "Crosby",
                "Cypress",
                "Deer Park",
                "Galena Park",
                "Highlands",
                "Hockley",
                "Houston",
                "Huffman",
                "Hufsmith",
                "Humble",
                "Katy",
                "Kingwood",
                "La Porte",
                "North Houston",
                "Pasadena",
                "Seabrook",
                "South Houston",
                "Spring",
                "Tomball",
                "Waller",
                "Webster",
            ],
            "county_name": "Harris County",
            "deaths": 8638,
            "fips": 48201,
            "icu_bed_capacity": 1453,
            "icu_bed_current_usage": 1358,
            "id": 355,
            "infection_rate": 0.81,
            "overall_risk_level_IGNORE": 2.0,
            "population": 4713325,
            "risk_level": "Medium",
            "test_positivity_ratio": 0.101,
            "vaccinations_completed_ratio": 0.543,
            "vaccinations_initiated_ratio": 0.632,
            "values_time_series": {
                "cases": {
                    "2021-10-01": 537393,
                    "2021-10-02": 539516,
                    "2021-10-03": 539852,
                    "2021-10-04": 540104,
                    "2021-10-05": 541184,
                    "2021-10-06": 543104,
                    "2021-10-07": 544136,
                    "2021-10-08": 544622,
                    "2021-10-09": 545647,
                    "2021-10-10": 546123,
                    "2021-10-11": 546322,
                    "2021-10-12": 547118,
                    "2021-10-13": 548812,
                    "2021-10-14": 549472,
                    "2021-10-15": 550421,
                    "2021-10-16": 550710,
                    "2021-10-17": 551134,
                    "2021-10-18": 551363,
                    "2021-10-19": 551751,
                    "2021-10-20": 552135,
                    "2021-10-21": 553061,
                    "2021-10-22": 553455,
                    "2021-10-23": 554031,
                    "2021-10-24": 554326,
                },
                "deaths": {
                    "2021-10-01": 8345,
                    "2021-10-02": 8392,
                    "2021-10-03": 8414,
                    "2021-10-04": 8419,
                    "2021-10-05": 8456,
                    "2021-10-06": 8502,
                    "2021-10-07": 8529,
                    "2021-10-08": 8579,
                    "2021-10-09": 8621,
                    "2021-10-10": 8638,
                    "2021-10-11": 8638,
                    "2021-10-12": 8672,
                    "2021-10-13": 8720,
                    "2021-10-14": 8758,
                    "2021-10-15": 8789,
                    "2021-10-16": 8820,
                    "2021-10-17": 8843,
                    "2021-10-18": 8845,
                    "2021-10-19": 8881,
                    "2021-10-20": 8915,
                    "2021-10-21": 8938,
                    "2021-10-22": 8972,
                    "2021-10-23": 9004,
                    "2021-10-24": 9021,
                },
            },
        }
        self.assertEqual(expect, data)

    def test_flight(self):
        r = requests.get("https://api.atraveltx.me/api/flight")
        self.assertEqual(r.status_code, 200)
        data = r.json()
        self.assertEqual(len(data["result"]), 1300)

    def test_flight_id(self):
        r = requests.get("https://api.atraveltx.me/api/flight/id=3")
        data = r.json()
        expect = {
            "airline_iata": "QR",
            "airline_icao": "QTR",
            "airline_name": "Qatar Airways",
            "arrival_airport": "Municipal",
            "arrival_baggage": "1A",
            "arrival_gate": "3",
            "arrival_iata": "ABI",
            "arrival_icao": "KABI",
            "arrival_scheduled": "2021-10-14T18:06:00",
            "arrival_terminal": None,
            "city": "Abilene",
            "codeshared_iata": "aa",
            "codeshared_icao": "aal",
            "codeshared_name": "american airlines",
            "codeshared_number": "4454",
            "country_code": "US",
            "county": "Taylor",
            "departure_airport": "Dallas/Fort Worth International",
            "departure_gate": "E37A",
            "departure_iata": "DFW",
            "departure_icao": "KDFW",
            "departure_scheduled": "2021-10-14T17:07:00",
            "departure_terminal": "E",
            "elevation": 1790.6,
            "flight_date": "2021-10-14",
            "flight_iata": "QR2751",
            "flight_icao": "QTR2751",
            "flight_number": "2751",
            "id": 3,
            "latitude": 32.4113,
            "longitude": -99.6819,
            "name": "Abilene Rgnl",
            "state": "TX",
            "timezone": "America/Chicago",
            "wiki_url": "http://en.wikipedia.org/wiki/Abilene_Regional_Airport",
        }

        self.assertEqual(expect, data)

    def test_flight_name(self):
        r = requests.get("https://api.atraveltx.me/api/flight/city=Waco")
        self.assertEqual(r.status_code, 200)
        data = r.json()
        self.assertEqual(len(data), 13)

    def test_flight_info(self):
        r = requests.get("https://api.atraveltx.me/api/flight/info")
        self.assertEqual(r.status_code, 200)
        data = r.json()
        self.assertEqual(len(data), 3)

    def test_city(self):
        r = requests.get("https://api.atraveltx.me/api/city")
        self.assertEqual(r.status_code, 200)
        data = r.json()
        self.assertEqual(len(data["result"]), 1151)

    def test_city_name(self):
        r = requests.get("https://api.atraveltx.me/api/city/city_name=Austin")
        data = r.json()
        expect = {
            "County_name": "Travis County",
            "Have_county": "Y",
            "Latitude": 30.27356522,
            "Longitude": -97.74447762,
            "average_temp": 16.73,
            "city_name": "Austin",
            "city_number": "2100",
            "id": 194,
            "population": 790390,
            "position": "South East",
            "sale_tax": 8.25,
            "size": "297.9",
            "wiki_history": "Austin, Travis County and Williamson County have been the site of human habitation since at least 9200 BC. The area's earliest known inhabitants lived during the late Pleistocene (Ice Age) and are linked to the Clovis culture around 9200 BC (over 11,200 years ago), based on evidence found throughout the area and documented at the much-studied Gault Site, midway between Georgetown and Fort Hood.\nWhen settlers arrived from Europe, the Tonkawa tribe inhabited the area. The Comanches and Lipan Apaches were also known to travel through the area. Spanish colonists, including the Espinosa-Olivares-Aguirre expedition, traveled through the area, though few permanent settlements were created for some time. In 1730, three missions from East Texas were combined and reestablished as one mission on the south side of the Colorado River, in what is now Zilker Park, in Austin. The mission was in this area for only about seven months, and then was moved to San Antonio de B\u00e9xar and split into three missions.\nDuring the 1830s, pioneers began to settle the area in central Austin along the Colorado River. Spanish forts were established in what are now Bastrop and San Marcos. Following Mexico's independence, new settlements were established in Central Texas, but growth in the region was stagnant because of conflicts with the regional Native Americans.\nIn 1835?1836, Texans fought and won independence from Mexico. Texas thus became an independent country with its own president, congress, and monetary system. After Vice President Mirabeau B. Lamar visited the area during a buffalo-hunting expedition between 1837 and 1838, he proposed that the republic's capital, then in Houston, be relocated to the area situated on the north bank of the Colorado River (near the present-day Congress Avenue Bridge). In 1839, the site was chosen to replace Houston as the capital of the Republic of Texas and was incorporated under the name 'Waterloo'. Shortly afterward, the name was changed to Austin in honor of Stephen F. Austin, the 'Father of Texas' and the republic's first secretary of state. The city grew throughout the 19th century and became a center for government and education with the construction of the Texas State Capitol and the University of Texas at Austin. After a severe lull in economic growth from the Great Depression, Austin resumed its steady development.\nIn 1839, the Texas Congress formed a commission to seek a site for a new capital to be named for Stephen F. Austin. Mirabeau B. Lamar, second president of the newly formed Republic of Texas, advised the commissioners to investigate the area named Waterloo, noting the area's hills, waterways, and pleasant surroundings. Waterloo was selected, and 'Austin' was chosen as the town's new name. The location was seen as a convenient crossroads for trade routes between Santa Fe and Galveston Bay, as well as routes between northern Mexico and the Red River.\nEdwin Waller was picked by Lamar to survey the village and draft a plan laying out the new capital. The original site was narrowed to 640 acres (260\u00a0ha) that fronted the Colorado River between two creeks, Shoal Creek and Waller Creek, which was later named in his honor. Waller and a team of surveyors developed Austin's first city plan, commonly known as the Waller Plan, dividing the site into a 14-block grid plan bisected by a broad north?south thoroughfare, Congress Avenue, running up from the river to Capital Square, where the new Texas State Capitol was to be constructed. A temporary one-story capitol was erected on the corner of Colorado and 8th Streets. On August 1, 1839, the first auction of 217 out of 306 lots total was held. The Waller Plan designed and surveyed now forms the basis of downtown Austin.\nIn 1840, a series of conflicts between the Texas Rangers and the Comanches, known as the Council House Fight and the Battle of Plum Creek, pushed the Comanches westward, mostly ending conflicts in Central Texas. Settlement in the area began to expand quickly. Travis County was established in 1840, and the surrounding counties were mostly established within the next two decades.\nInitially, the new capital thrived but Lamar's political enemy, Sam Houston, used two Mexican army incursions to San Antonio as an excuse to move the government. Sam Houston fought bitterly against Lamar's decision to establish the capital in such a remote wilderness. The men and women who traveled mainly from Houston to conduct government business were intensely disappointed as well. By 1840, the population had risen to 856, nearly half of whom fled Austin when Congress recessed. The resident African American population listed in January of this same year was 176. The fear of Austin's proximity to the Indians and Mexico, which still considered Texas a part of their land, created an immense motive for Sam Houston, the first and third President of the Republic of Texas, to relocate the capital once again in 1841. Upon threats of Mexican troops in Texas, Houston raided the Land Office to transfer all official documents to Houston for safe keeping in what was later known as the Archive War, but the people of Austin would not allow this unaccompanied decision to be executed. The documents stayed, but the capital would temporarily move from Austin to Houston to Washington-on-the-Brazos. Without the governmental body, Austin's population declined to a low of only a few hundred people throughout the early 1840s. The voting by the fourth President of the Republic, Anson Jones, and Congress, who reconvened in Austin in 1845, settled the issue to keep Austin the seat of government, as well as annex the Republic of Texas into the United States.\nIn 1860, 38% of Travis County residents were slaves. In 1861, with the outbreak of the American Civil War, voters in Austin and other Central Texas communities voted against secession. However, as the war progressed and fears of attack by Union forces increased, Austin contributed hundreds of men to the Confederate forces. The African American population of Austin swelled dramatically after the enforcement of the Emancipation Proclamation in Texas by Union General Gordon Granger at Galveston, in an event commemorated as Juneteenth. Black communities such as Wheatville, Pleasant Hill, and Clarksville were established, with Clarksville being the oldest surviving freedomtown - the original post-Civil War settlements founded by former African-American slaves - west of the Mississippi River. In 1870, blacks made up 36.5% of Austin's population.\nThe postwar period saw dramatic population and economic growth. The opening of the Houston and Texas Central Railway (H&TC) in 1871 turned Austin into the major trading center for the region, with the ability to transport both cotton and cattle. The Missouri, Kansas &amp; Texas (MKT) line followed close behind. Austin was also the terminus of the southernmost leg of the Chisholm Trail, and 'drovers' pushed cattle north to the railroad. Cotton was one of the few crops produced locally for export, and a cotton gin engine was located downtown near the trains for 'ginning' cotton of its seeds and turning the product into bales for shipment. However, as other new railroads were built through the region in the 1870s, Austin began to lose its primacy in trade to the surrounding communities. In addition, the areas east of Austin took over cattle and cotton production from Austin, especially in towns like Hutto and Taylor that sit over the blackland prairie, with its deep, rich soils for producing cotton and hay.\nIn September 1881, Austin public schools held their first classes. The same year, Tillotson Collegiate and Normal Institute (now part of Huston?Tillotson University) opened its doors. The University of Texas held its first classes in 1883, although classes had been held in the original wooden state capitol for four years before.\nDuring the 1880s, Austin gained new prominence as the state capitol building was completed in 1888 and claimed as the seventh largest building in the world. In the late 19th century, Austin expanded its city limits to more than three times its former area, and the first granite dam was built on the Colorado River to power a new street car line and the new 'moon towers'. The first dam washed away in a flood on April 7, 1900.\nIn the late 1920s and 1930s, Austin implemented the 1928 Austin city plan through a series of civic development and beautification projects that created much of the city's infrastructure and many of its parks. In addition, the state legislature established the Lower Colorado River Authority (LCRA) that, along with the city of Austin, created the system of dams along the Colorado River to form the Highland Lakes. These projects were enabled in large part because the Public Works Administration provided Austin with greater funding for municipal construction projects than other Texas cities.\nDuring the early twentieth century, a three-way system of social segregation emerged in Austin, with Anglos, African Americans and Mexicans being separated by custom or law in most aspects of life, including housing, health care, and education. Many of the municipal improvement programs initiated during this period?such as the construction of new roads, schools, and hospitals?were deliberately designed to institutionalize this system of segregation. Deed restrictions also played an important role in residential segregation. After 1935 most housing deeds prohibited African Americans (and sometimes other nonwhite groups) from using land. Combined with the system of segregated public services, racial segregation increased in Austin during the first half of the twentieth century, with African Americans and Mexicans experiencing high levels of discrimination and social marginalization.\nIn 1940, the destroyed granite dam on the Colorado River was finally replaced by a hollow concrete dam that formed Lake McDonald (now called Lake Austin) and which has withstood all floods since. In addition, the much larger Mansfield Dam was built by the LCRA upstream of Austin to form Lake Travis, a flood-control reservoir.\n\n In the early 20th century, the Texas Oil Boom took hold, creating tremendous economic opportunities in Southeast Texas and North Texas. The growth generated by this boom largely passed by Austin at first, with the city slipping from fourth largest to 10th largest in Texas between 1880 and 1920.\nAfter the mid-20th century, Austin became established as one of Texas' major metropolitan centers. In 1970, the U.S. Census Bureau reported Austin's population as 14.5% Hispanic, 11.9% black, and 73.4% non-Hispanic white. In the late 20th century, Austin emerged as an important high tech center for semiconductors and software. The University of Texas at Austin emerged as a major university.\nThe 1970s saw Austin's emergence in the national music scene, with local artists such as Willie Nelson, Asleep at the Wheel, and Stevie Ray Vaughan and iconic music venues such as the Armadillo World Headquarters. Over time, the long-running television program , its namesake Austin City Limits Festival, and the South by Southwest music festival solidified the city's place in the music industry.\nAustin, the southernmost state capital of the contiguous 48 states, is located in Central Texas on the Colorado River. Austin is 146 miles (230\u00a0km) northwest of Houston, 182 miles (290\u00a0km) south of Dallas and 74 miles (120\u00a0km) northeast of San Antonio.\nIn 2010, the city occupied a total area of 305.1 square miles (790.1\u00a0km). Approximately 7.2 square miles (18.6\u00a0km) of this area is water.  Austin is situated at the foot of the Balcones Escarpment, on the Colorado River, with three artificial lakes within the city limits: Lady Bird Lake (formerly known as Town Lake), Lake Austin (both created by dams along the Colorado River), and Lake Walter E. Long that is partly used for cooling water for the Decker Power Plant. Mansfield Dam and the foot of Lake Travis are located within the city's limits. Lady Bird Lake, Lake Austin, and Lake Travis are each on the Colorado River.\nThe elevation of Austin varies from 425 feet (130\u00a0m) to approximately 1,000 feet (305\u00a0m) above sea level. Due to the fact it straddles the Balcones Fault, much of the eastern part of the city is flat, with heavy clay and loam soils, whereas the western part and western suburbs consist of rolling hills on the edge of the Texas Hill Country. Because the hills to the west are primarily limestone rock with a thin covering of topsoil, portions of the city are frequently subjected to flash floods from the runoff caused by thunderstorms. To help control this runoff and to generate hydroelectric power, the Lower Colorado River Authority operates a series of dams that form the Texas Highland Lakes. The lakes also provide venues for boating, swimming, and other forms of recreation within several parks on the lake shores.\nAustin is located at the intersection of four major ecological regions, and is consequently a temperate-to-hot green oasis with a highly variable climate having some characteristics of the desert, the tropics, and a wetter climate. The area is very diverse ecologically and biologically, and is home to a variety of animals and plants. Notably, the area is home to many types of wildflowers that blossom throughout the year but especially in the spring. This includes the popular bluebonnets, some planted by 'Lady Bird' Johnson, wife of former President Lyndon B. Johnson.\nThe soils of Austin range from shallow, gravelly clay loams over limestone in the western outskirts to deep, fine sandy loams, silty clay loams, silty clays or clays in the city's eastern part. Some of the clays have pronounced shrink-swell properties and are difficult to work under most moisture conditions. Many of Austin's soils, especially the clay-rich types, are slightly to moderately alkaline and have free calcium carbonate.\nAustin's skyline historically was modest, dominated by the Texas State Capitol and the University of Texas Main Building. However, since the 2000s, many new high-rise towers have been constructed. Austin is currently undergoing a skyscraper boom, which includes recent construction on new office, hotel and residential buildings. Downtown's buildings are somewhat spread out, partly due to a set of zoning restrictions that preserve the view of the Texas State Capitol from various locations around Austin, known as the Capitol View Corridors.\nAt night, parts of Austin are lit by 'artificial moonlight' from moonlight towers built to illuminate the central part of the city. The 165-foot (50\u00a0m) moonlight towers were built in the late 19th century and are now recognized as historic landmarks. Only 15 of the 31 original innovative towers remain standing in Austin, but none remain in any of the other cities where they were installed. The towers are featured in the 1993 film .\nThe central business district of Austin is home to the tallest condo towers in the state, with The Independent (58 stories and 690\u00a0ft (210\u00a0m) tall) and The Austonian (topping out at 56 floors and 685\u00a0ft (209\u00a0m) tall). The Independent became the tallest all-residential building in the U.S. west of Chicago when topped out in 2018. In 2005, then-Mayor Will Wynn set out a goal of having 25,000 people living downtown by 2015. Although downtown's growth did not meet this goal, downtown's residential population did surge from an estimated 5,000 in 2005 to 12,000 in 2015. The skyline has drastically changed in recent years, and the residential real estate market has remained relatively strong. As of December\u00a02016, there were 31 high rise projects either under construction, approved or planned to be completed in Austin's downtown core between 2017 and 2020. Sixteen of those were set to rise above 400\u00a0ft (120\u00a0m) tall, including four above 600', and eight above 500'. An additional 15 towers were slated to stand between 300' and 399' tall.\nAustin is located within the middle of a unique, narrow transitional zone between the dry deserts of the American Southwest and the lush, green, more humid regions of the American Southeast. Its climate, topography, and vegetation share characteristics of both. Officially, Austin has a humid subtropical climate under the K\u00f6ppen climate classification. This climate is typified by very long and hot summers, short and mild winters, and pleasantly warm spring and fall seasons in-between. Austin averages 34.32 inches (872\u00a0mm) of annual rainfall distributed mostly evenly throughout the year, though spring and fall are the wettest seasons. Sunshine is common during all seasons, with 2,650 hours, or 60.3% of the possible total, of bright sunshine per year. Austin falls in USDA hardiness zones 8b (15\u00a0\u00b0F to 20\u00a0\u00b0F) and 9a (20\u00a0\u00b0F to 25\u00a0\u00b0F).\nSummers in Austin are very hot, with average July and August highs frequently reaching the high-90s (34?36\u00a0\u00b0C) or above. Highs reach 90\u00a0\u00b0F (32\u00a0\u00b0C) on 116 days per year, of which 18 days reach 100\u00a0\u00b0F (38\u00a0\u00b0C). The average daytime high is 70\u00a0\u00b0F (21\u00a0\u00b0C) or warmer between March 6 and November 20, rising to 80\u00a0\u00b0F (27\u00a0\u00b0C) or warmer between April 14 and October 24, and reaching 90\u00a0\u00b0F (32\u00a0\u00b0C) or warmer between May 30 and September 18. The highest ever recorded temperature was 112\u00a0\u00b0F (44\u00a0\u00b0C) occurring on September 5, 2000, and August 28, 2011. An uncommon characteristic of Austin's climate is its highly variable humidity, which fluctuates frequently depending on the shifting patterns of air flow and wind direction. It is common for a lengthy series of warm, dry, low-humidity days to be occasionally interrupted by very warm and humid days, and vice versa. Humidity rises with winds from the east or southeast, when the air drifts inland from the Gulf of Mexico, but decreases significantly with winds from the west or southwest, bringing air flowing from Chihuahuan Desert areas of West Texas or northern Mexico.\nWinters in Austin are mild with cool nights, although occasional short-lived bursts of cold weather known as 'Blue Northers' can occur. January is the coolest month with an average daytime high of 61\u00a0\u00b0F (16\u00a0\u00b0C). The overnight low drops to or below freezing 19 times per year, and sinks below 45\u00a0\u00b0F (7\u00a0\u00b0C) during 88 evenings per year, including most nights between mid-December and mid-February. Lows in the upper 30s also occur commonly during the winter. Conversely, winter months are also capable of occasionally producing warm days. On average, eight days in January reach or exceed 70\u00a0\u00b0F (21\u00a0\u00b0C) and one day reaches 80\u00a0\u00b0F (27\u00a0\u00b0C). The lowest ever recorded temperature in the city was -2\u00a0\u00b0F (-19\u00a0\u00b0C) on January 31, 1949. Roughly every two years Austin experiences an ice storm that freezes roads over and cripples travel in the city for 24 to 48 hours. When Austin received 0.04 inches (1\u00a0mm) of ice on January 24, 2014, there were 278 vehicular collisions. Similarly, snowfall is rare in Austin. A snow event of 0.9 inches (2\u00a0cm) on February 4, 2011, caused more than 300 car crashes. The most recent major snow event occurred the week of February 14, 2021, when as many as 7.5 inches were recorded in parts of Travis County. \nTypical of Central Texas, severe weather in Austin is a threat that can strike during any season. However, it is most common during the spring. According to most classifications, Austin lies within the extreme southern periphery of Tornado Alley, although many sources place Austin outside of Tornado Alley altogether. Consequently, tornadoes strike Austin less frequently than areas farther to the north. However, severe weather and/or supercell thunderstorms can occur multiple times per year, bringing damaging winds, lightning, heavy rain, and occasional flash flooding to the city. The deadliest storm to ever strike city limits was the twin tornadoes storm of May 4, 1922, while the deadliest tornado outbreak to ever strike the metro area was the Central Texas tornado outbreak of May 27, 1997.\nFrom October 2010 through September 2011, both major reporting stations in Austin, Camp Mabry and Bergstrom Int'l, had the least rainfall of a water year on record, receiving less than a third of normal precipitation. This was a result of La Ni\u00f1a conditions in the eastern Pacific Ocean where water was significantly cooler than normal. David Brown, a regional official with the National Oceanic and Atmospheric Administration, explained that 'these kinds of droughts will have effects that are even more extreme in the future, given a warming and drying regional climate.' The drought, coupled with exceedingly high temperatures throughout the summer of 2011, caused many wildfires throughout Texas, including notably the Bastrop County Complex Fire in neighboring Bastrop, TX.\nIn Fall 2018, Austin and surrounding areas received heavy rainfall and flash flooding following Hurricane Sergio. The Lower Colorado River Authority opened four floodgates of the Mansfield Dam after Lake Travis was recorded at 146% full at 704.3 feet (214.7\u00a0m). From the October 22 to 29, 2018 the City of Austin issued a mandatory citywide boil-water advisory after the Highland Lakes, home to the city's main water supply, became overwhelmed by unprecedented amounts of silt, dirt, and debris that washed in from the Llano River. Austin Water, the city's water utility, has the capacity to process up to 300\u00a0million gallons of water per day, but the elevated level of turbidity reduced output to only 105\u00a0million gallons per day since Austin residents consumed an average of 120\u00a0million gallons of water per day, so the infrastructure was not able to keep up with demand.\nAccording to the 2010 United States census, the racial composition of Austin was 68.3% White (48.7% non-Hispanic whites), 35.1% Hispanic or Latino (29.1% Mexican, 0.5% Puerto Rican, 0.4% Cuban, 5.1% Other), 8.1% African American, 6.3% Asian (1.9% Indian, 1.5% Chinese, 1.0% Vietnamese, 0.7% Korean, 0.3% Filipino, 0.2% Japanese, 0.8% Other), 0.9% American Indian, 0.1% Native Hawaiian and Other Pacific Islander, and 3.4% two or more races.\nAt the 2000 United States Census, there were 656,562 people, 265,649 households, and 141,590 families residing in the city. The population density was 2,610.4 inhabitants per square mile (1,007.9/km). There were 276,842 housing units at an average density of 1,100.7 per square mile (425.0/km). There were 265,648 households, out of which 26.8% had children under the age of 18 living with them, 38.1% were married couples living together, 10.8% had a female householder with no husband present, and 46.7% were non-families. 32.8% of all households were made up of individuals, and 4.6% had someone living alone who was 65 years of age or older. The average household size was 2.40 and the average family size was 3.14.\nIn the city, the population was spread out, with 22.5% under the age of 18, 16.6% from 18 to 24, 37.1% from 25 to 44, 17.1% from 45 to 64, and 6.7% who were 65 years of age or older. The median age was 30 years. For every 100 females, there were 105.8 males.\nThe median income for a household in the city was US$42,689, and the median income for a family was $54,091. Males had a median income of $35,545 vs. $30,046 for females. The per capita income for the city was $24,163. About 9.1% of families and 14.4% of the population were below the poverty line, including 16.5% of those under age 18 and 8.7% of those age 65 or over. The median house price was $185,906 in 2009, and it has increased every year since 2004. The median value of a house which the owner occupies was $227,800 in 2014 -- higher than the average American home value of $175,700.\nA 2014 University of Texas study stated that Austin was the only U.S. city with a fast growth rate between 2000 and 2010 with a net loss in African Americans. As of 2014, Austin's African American and non-Hispanic white percentage share of the total population was declining despite the actual numbers of both ethnic groups increasing, as the rapid growth of the Latino or Hispanic and Asian populations has outpaced all other ethnic groups in the city. Austin's non-Hispanic white population first dropped below 50% in 2005.\nAccording to a survey completed in 2014 by Gallup, it is estimated that 5.3% of residents in the Austin metropolitan area identify as lesbian, gay, bisexual, or transgender. The Austin metropolitan area had the third-highest rate in the nation.\nAccording to Sperling's BestPlaces, 52.4% of Austin's population are religious. The majority of Austinites identified themselves as Christians, about 25.2% of whom claimed affiliation with the Catholic Church. The city's Catholic population is served by the Roman Catholic Diocese of Austin, headquartered at the Cathedral of Saint Mary. Nationwide, 23% of Americans identified as Catholic in 2016. Other significant Christian groups in Austin include Baptists (8.7%), followed by Methodists (4.3%), Latter-Day Saints (1.5%), Episcopalians or Anglicans (1.0%), Lutherans (0.8%), Presbyterians (0.6%), Pentecostals (0.3%), and other Christians such as the Disciples of Christ and Eastern Orthodox Church (7.1%). The second largest religion Austinites identify with is Islam (1.7%); roughly 0.8% of Americans nationwide claimed affiliation with the Islamic faith. The dominant branch of Islam is Sunni Islam. Established in 1977, the largest mosque in Austin is the Islamic Center of Greater Austin. The community is affiliated with the Islamic Society of North America. The same study says that eastern faiths including Buddhism, Sikhism, and Hinduism made up 0.9% of the city's religious population. Several Hindu temples exist in the Austin Metropolitan area with the most notable one being Radha Madhav Dham. Judaism forms less than 0.1% of the religious demographic in Austin. Orthodox, Reform, and Conservative congregations are present in the community. In addition to those religious groups, Austin is also home to an active secular humanist community, hosting nationwide television shows and charity work.\nAs of 2019, there were 2,255 individuals experiencing homelessness in Travis County. Of those, 1,169 were sheltered and 1,086 were unsheltered. In September 2019, the Austin City Council approved $62.7\u00a0million for programs aimed at homelessness, which includes housing displacement prevention, crisis mitigation, and affordable housing; the city council also earmarked $500,000 for crisis services and encampment cleanups.\nIn June 2019, following a federal court ruling on homelessness sleeping in public, the Austin City Council lifted a 25-year-old ban on camping, sitting, or lying down in public unless doing so causes an obstruction. The resolution also included the approval of a new housing-focused shelter in South Austin. In early October 2019, Texas Governor Greg Abbott sent a letter to Mayor Steve Adler threatening to deploy state resources to combat the camping ban repeal. On October 17, 2019, the City Council revised the camping ordinance, which imposed increased restrictions on sidewalk camping. In November 2019, the State of Texas opened a temporary homeless encampment on a former vehicle storage yard owned by the Texas Department of Transportation.\nIn May 2021, the camping ban was reinstated after a ballot proposition was approved by 57% of voters. The ban introduces penalties for camping, sitting, or lying down on a public sidewalk or sleeping outdoors in or near Downtown Austin or the area around the University of Texas campus. The ordinance would also prohibit solicitation at certain locations.\nThe Greater Austin metropolitan statistical area had a gross domestic product (GDP) of $86\u00a0billion in 2010. Austin is considered to be a major center for high tech. Thousands of graduates each year from the engineering and computer science programs at the University of Texas at Austin provide a steady source of employees that help to fuel Austin's technology and defense industry sectors. The region's rapid growth has led  to rank the Austin metropolitan area number one among all big cities for jobs for 2012 in their annual survey and WSJ Marketwatch to rank the area number one for growing businesses. By 2013, Austin was ranked No. 14 on  list of the Best Places for Business and Careers (directly below Dallas, No. 13 on the list). As a result of the high concentration of high-tech companies in the region, Austin was strongly affected by the dot-com boom in the late 1990s and subsequent bust. Austin's largest employers include the Austin Independent School District, the City of Austin, Dell, the U.S. Federal Government, NXP Semiconductors, IBM, St. David's Healthcare Partnership, Seton Family of Hospitals, the State of Texas, the Texas State University, and the University of Texas at Austin.\nOther high-tech companies with operations in Austin include 3M, Apple, Amazon, AMD, Apartment Ratings, Applied Materials, Arm Holdings, Bigcommerce, BioWare, Blizzard Entertainment, Buffalo Technology, Cirrus Logic, Cisco Systems, Dropbox, eBay, Electronic Arts, Flextronics, Facebook, Google, Hewlett-Packard, Hoover's, HomeAway, HostGator, Intel Corporation, National Instruments, Nintendo, Nvidia, Oracle, PayPal, Polycom, Qualcomm, Rackspace, RetailMeNot, Rooster Teeth, Samsung Group, Silicon Laboratories, Spansion, Tesla, United Devices, VMware, and Xerox. In 2010, Facebook accepted a grant to build a downtown office that could bring as many as 200 jobs to the city. The proliferation of technology companies has led to the region's nickname, 'Silicon Hills', and spurred development that greatly expanded the city.\nAustin is also emerging as a hub for pharmaceutical and biotechnology companies; the city is home to about 85 of them. In 2004, the city was ranked by the Milken Institute as the #12 biotech and life science center in the United States and in 2018, CBRE Group ranked Austin as #3 emerging life sciences cluster Companies such as Hospira, Pharmaceutical Product Development, and ArthroCare Corporation are located there.\nWhole Foods Market, an international grocery store chain specializing in fresh and packaged food products, was founded and is headquartered in Austin.\nOther companies based in Austin include NXP Semiconductors, GoodPop, Temple-Inland, Sweet Leaf Tea Company, Keller Williams Realty, National Western Life, GSD&amp;M, Dimensional Fund Advisors, Golfsmith, Forestar Group, EZCorp, Outdoor Voices, Tito's Vodka, Indeed, Speak Social, and YETI.\nIn 2018, Austin metro-area companies saw a total of $1.33\u00a0billion invested. Austin's VC numbers were so strong in 2018 that they accounted for more than 60 percent of Texas' total investments.\n'Keep Austin Weird' has been a local motto for years, featured on bumper stickers and T-shirts. This motto has not only been used in promoting Austin's eccentricity and diversity, but is also meant to bolster support of local independent businesses. According to the 2010 book  the phrase was begun by a local Austin Community College librarian, Red Wassenich, and his wife, Karen Pavelka, who were concerned about Austin's 'rapid descent into commercialism and overdevelopment.' The slogan has been interpreted many ways since its inception, but remains an important symbol for many Austinites who wish to voice concerns over rapid growth and irresponsible development. Austin has a long history of vocal citizen resistance to development projects perceived to degrade the environment, or to threaten the natural and cultural landscapes.\nAccording to the Nielsen Company, adults in Austin read and contribute to blogs more than those in any other U.S. metropolitan area. Austin residents have the highest Internet usage in all of Texas. In 2013, Austin was the most active city on Reddit, having the largest number of views per capita. Austin was selected as the No. 2 Best Big City in 'Best Places to Live' by  magazine in 2006, and No. 3 in 2009, and also the 'Greenest City in America' by MSN.\nSouth Congress is a shopping district stretching down South Congress Avenue from Downtown. This area is home to coffee shops, eccentric stores, restaurants, food trucks, trailers, and festivals. It prides itself on 'Keeping Austin Weird,' especially with development in the surrounding area(s). Many Austinites attribute its enduring popularity to the magnificent and unobstructed view of the Texas State Capitol.\nThe Rainey Street Historic District is a neighborhood in Downtown Austin consisting mostly of bungalow style homes built in the early 20th century. Since the early 2010s, the former working class residential street has turned into a popular nightlife district. Much of the historic homes have been renovated into bars and restaurants, many of which feature large porches and outdoor yards for patrons. The Rainey Street district is also ",
            "wiki_link": "https://en.wikipedia.org/wiki/Austin,_Texas",
        }

        self.assertEqual(expect, data)


if __name__ == "__main__":
    unittest.main()
